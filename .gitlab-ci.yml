include:
  - '/support/docker/tuxmake-images.yml'
  - '/support/docker/tuxmake-ci-images.yml'

image:
  name: $CI_REGISTRY/linaro/tuxmake/ci-debian-11

variables:
  LANG: C.UTF-8

.tuxmake:
  except:
    - schedules

.arm64:
  only:
    - branches@Linaro/tuxmake
  tags:
    - arm64

stages:
  - build-ci-images
  - publish-ci-images
  - unit-tests
  - code-checks
  - integration-tests
  - build-base
  - publish-base
  - build
  - publish


.unit-tests:
  extends: .tuxmake
  needs: []
  stage: unit-tests
  script:
    - make unit-tests

unit-tests-python3.10:
  extends: .unit-tests
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-22.04

unit-tests-python3.9:
  extends: .unit-tests
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-11

unit-tests-python3.8:
  extends: .unit-tests
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-20.04

unit-tests-python3.7:
  extends: .unit-tests
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-10

unit-tests-python3.6:
  extends: .unit-tests
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-18.04

unit-tests-python3.10-arm64:
  extends: [.unit-tests, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-22.04

unit-tests-python3.9-arm64:
  extends: [.unit-tests, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-11

unit-tests-python3.8-arm64:
  extends: [.unit-tests, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-20.04

unit-tests-python3.7-arm64:
  extends: [.unit-tests, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-10

unit-tests-python3.6-arm64:
  extends: [.unit-tests, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-18.04

.code-checks:
  extends: .tuxmake
  stage: code-checks
  needs: []

style:
  extends: .code-checks
  script:
    - make style

typecheck:
  extends: .code-checks
  script:
    - make typecheck

codespell:
  extends: .code-checks
  script:
    - make codespell

docker-build-tests:
  extends: .code-checks
  script:
    - make docker-build-tests

.integration:
  extends: .tuxmake
  stage: integration-tests
  needs: []
  before_script:
    - git status
    - rm -rf dist/
    - flit --debug build
    - python3 -m pip install dist/*.whl
  script:
    - make integration-tests TMV=1

integration-python3.9:
  extends: .integration
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-11

integration-python3.8:
  extends: .integration
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-20.04

integration-python3.7:
  extends: .integration
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-10

integration-python3.6:
  extends: .integration
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-10

integration-python3.9-arm64:
  extends: [.integration, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-11

integration-python3.8-arm64:
  extends: [.integration, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-20.04

integration-python3.7-arm64:
  extends: [.integration, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-10

integration-python3.6-arm64:
  extends: [.integration, .arm64]
  image: $CI_REGISTRY/linaro/tuxmake/ci-ubuntu-18.04

integration-docker:
  extends: integration-python3.8
  services:
    - name: docker:20.10-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
  script:
    # Run integration tests as a non-root user, on purpose. HOME and TMPDIR
    # must be inside $(pwd) so that they are available to the docker server
    # (which is running in a separate container)
    - mkdir -p nobody/home nobody/tmp
    - chown -R nobody:nogroup nobody
    - runuser -u nobody make integration-tests-docker TMV=1 HOME=$(pwd)/nobody TMPDIR=$(pwd)/nobody/tmp

build-docs:
  extends: .tuxmake
  stage: build
  needs: []
  image: $CI_REGISTRY/linaro/tuxmake/ci-debian-11
  before_script:
    - apt-get update -q
    - python3 -m pip install mkautodoc mkdocs-material  # FIXME not in Debian
  script:
    - make man
    - make doc
  artifacts:
    paths:
      - public

pages:
  stage: publish
  needs:
    - build-docs
    - packages
  artifacts:
    paths:
      - public
  only:
    - tags
  script:
    # TUXMAKE_RELEASE_MANAGERS_GPG_KEYS is defined on gitlab, in the CI/CD
    # settings, section "Variables"
    - for key in ${TUXMAKE_RELEASE_MANAGERS_GPG_KEYS}; do wget -O - https://keys.openpgp.org/vks/v1/by-fingerprint/${key} | gpg --import; done
    - git tag --verify ${CI_COMMIT_TAG}
    - cp -r dist/repo public/packages

rpm:
  extends: .tuxmake
  stage: integration-tests
  image: fedora
  script:
    - dnf install -y dnf-plugins-core rpm-build crontabs shunit2 gcc-x86_64-linux-gnu gcc-aarch64-linux-gnu ccache clang lld which
    - dnf builddep -y tuxmake.spec
    - make rpm
    - rpm --install dist/tuxmake*.rpm
    - make integration-tests
  artifacts:
    paths:
      - dist/*.rpm

deb:
  extends: .tuxmake
  stage: integration-tests
  image: debian:testing
  script:
    - apt-get update
    - apt-get install -qy auto-apt-proxy
    - apt-get install -qy shunit2 gcc-x86-64-linux-gnu gcc-aarch64-linux-gnu ccache clang lld
    - apt-get build-dep -qy .
    - make deb
    - apt-get install -qy ./dist/tuxmake*.deb
    - make integration-tests
  artifacts:
    paths:
      - dist/*.deb

pkg:
  extends: .tuxmake
  stage: integration-tests
  image: archlinux:base-devel
  script:
    - sed -i '/^NoExtract/d' /etc/pacman.conf
    - pacman -Syyu --noconfirm
    - pacman -S --noconfirm git glibc python-flit
    - echo "C.UTF-8 UTF-8" >/etc/locale.gen
    - locale-gen
    - useradd -m build
    - "echo 'build ALL=(ALL) NOPASSWD: ALL' >/etc/sudoers.d/build"
    - su build -c "git config --global --add safe.directory ${CI_PROJECT_DIR}; make pkg"
    - pacman -U --noconfirm dist/*.pkg.tar.zst
    - su build -c "git clone https://aur.archlinux.org/shunit2.git && cd shunit2 && makepkg --noconfirm -irs"
    - pacman -S --noconfirm aarch64-linux-gnu-gcc ccache clang diffoscope libfaketime lld llvm socat
    - for binary in as gcc ld; do ln -sv /usr/bin/$binary /usr/bin/x86_64-linux-gnu-$binary; done
    - make integration-tests
  artifacts:
    paths:
      - dist/*.pkg.tar.zst

packages:
  image: debian:bullseye
  stage: build
  extends: .tuxmake
  only:
    - tags
  needs:
    - deb
    - rpm
  script:
    - apt-get update
    - apt-get install --no-install-recommends -qy dpkg-dev apt-utils createrepo-c rpm gpg gpg-agent
    - gpg --batch --import ${TUXMAKE_RELEASE_KEY}
    - ./scripts/package-repos
  artifacts:
    paths:
      - dist/repo
